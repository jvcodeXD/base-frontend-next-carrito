'use client'
import Typography from '@mui/material/Typography'
import { ReactNode, useEffect, useState } from 'react'
import { useAlerts, useSession } from '@/hooks'
import { ProductoCRUDType } from '@/app/admin/(configuracion)/productos/types/productosCRUDTypes'
import { useAuth } from '@/context/AuthProvider'
import { CasbinTypes } from '@/types'
import { Grid, useMediaQuery, useTheme } from '@mui/material'
import { delay, InterpreteMensajes, siteName } from '@/utils'
import { Constantes } from '@/config/Constantes'
import { imprimir } from '@/utils/imprimir'
import { usePathname } from 'next/navigation'
import { CriterioOrdenType } from '@/types/ordenTypes'
import { IconoTooltip } from '@/components/botones/IconoTooltip'
import { BotonBuscar } from '@/components/botones/BotonBuscar'
import { BotonOrdenar } from '@/components/botones/BotonOrdenar'
import { ordenFiltrado } from '@/utils/orden'
import { Paginacion } from '@/components/datatable/Paginacion'
import { CustomDataTable } from '@/components/datatable/CustomDataTable'
import { FiltroProductos } from '@/app/admin/(configuracion)/productos/ui/FiltroProductos'
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart'
import { CustomDialog } from '@/components/modales/CustomDialog'
import { VistaModalDetallePedido } from '../pedidos/ui/ModalDetallePedido'

export default function ProductosPage() {
  const [productosData, setProductosData] = useState<ProductoCRUDType[]>([])
  const [loading, setLoading] = useState<boolean>(true)

  // Hook para mostrar alertas
  const { Alerta } = useAlerts()
  const [errorProductosData, setErrorProductosData] = useState<any>()

  const [modalDetallePedido, setModalDetallePedido] = useState(false)
  
  const [productoEdicion, setProductoEdicion] = useState<
    ProductoCRUDType | undefined | null
  >()

  // Variables de páginado
  const [limite, setLimite] = useState<number>(10)
  const [pagina, setPagina] = useState<number>(1)
  const [total, setTotal] = useState<number>(0)

  const { sesionPeticion } = useSession()
  const { permisoUsuario, usuario } = useAuth()

  const [filtroProducto, setFiltroProducto] = useState<string>('')
  const [mostrarFiltroProductos, setMostrarFiltroProductos] = useState(false)
  
  // Permisos para acciones
  const [permisos, setPermisos] = useState<CasbinTypes>({
    read: false,
    create: false,
    update: false,
    delete: false,
  })

  const theme = useTheme()
  const xs = useMediaQuery(theme.breakpoints.only('xs'))

  // router para conocer la ruta actual
  const pathname = usePathname()

  /// Criterios de orden
  const [ordenCriterios, setOrdenCriterios] = useState<
    Array<CriterioOrdenType>
  >([
    { campo: 'codigo', nombre: 'Código', ordenar: true },
    { campo: 'nombre', nombre: 'Nombre', ordenar: true },
    { campo: 'descripcion', nombre: 'Descripción', ordenar: true },
    { campo: 'precio', nombre: 'Precio', ordenar: true },
    { campo: 'cantidad', nombre: 'Cantidad', ordenar: true },
    { campo: 'acciones', nombre: 'Acciones' },
  ])

  const contenidoTabla: Array<Array<ReactNode>> = productosData.map(
    (productoData, indexProducto) => [
      <Typography
        key={`${productoData.id}-${indexProducto}-codigo`}
        variant={'body2'}
      >{`${productoData.codigo}`}</Typography>,
      <Typography
        key={`${productoData.id}-${indexProducto}-nombre`}
        variant={'body2'}
      >
        {`${productoData.nombre}`}
      </Typography>,
      <Typography
        key={`${productoData.id}-${indexProducto}-descripcion`}
        variant={'body2'}
      >{`${productoData.descripcion}`}</Typography>,
      <Typography
        key={`${productoData.id}-${indexProducto}-grupo`}
        variant={'body2'}
      >{`${productoData.precio}`}</Typography>,
      <Typography
      key={`${productoData.id}-${indexProducto}-grupo`}
      variant={'body2'}
    >{`${productoData.cantidad}`}</Typography>,

      <Grid key={`${productoData.id}-${indexProducto}-acciones`}>

        {( // permisos.update &&  
          <IconoTooltip
            id={`editarProductos-${productoData.id}`}
            name={'Productos'}
            titulo={'Agregar a carrito'}
            color={'primary'}
            accion={() => {
              imprimir(`Agregaremos`, productoData)
              agregarDetallePedidoModal(productoData)
            }}
            icono={<ShoppingCartIcon/>}
          />
        )}
      </Grid>,
    ]
  )

  const acciones: Array<ReactNode> = [
    <BotonBuscar
      id={'accionFiltrarProductosToggle'}
      key={'accionFiltrarProductosToggle'}
      seleccionado={mostrarFiltroProductos}
      cambiar={setMostrarFiltroProductos}
    />,
    xs && (
      <BotonOrdenar
        id={'ordenarProductos'}
        key={`ordenarProductos`}
        label={'Ordenar productos'}
        criterios={ordenCriterios}
        cambioCriterios={setOrdenCriterios}
      />
    ),
  ]

  const obtenerProductosPeticion = async () => {
    try {
      setLoading(true)

      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/productos`,
        params: {
          pagina: pagina,
          limite: limite,
          ...(filtroProducto.length == 0 ? {} : { filtro: filtroProducto }),
          ...(ordenFiltrado(ordenCriterios).length == 0
            ? {}
            : {
                orden: ordenFiltrado(ordenCriterios).join(','),
              }),
        },
      })
      setProductosData(respuesta.datos?.filas)
      setTotal(respuesta.datos?.total)
      setErrorProductosData(null)
    } catch (e) {
      imprimir(`Error al obtener productos`, e)
      setErrorProductosData(e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    } finally {
      setLoading(false)
    }
  }

  const agregarDetallePedidoModal = (producto: ProductoCRUDType) => {
    setModalDetallePedido(true)
    setProductoEdicion(producto)
  }

  const cerrarModalDetallePedido = async () => {
    setModalDetallePedido(false)
    await delay(500)
  }

  async function definirPermisos() {
    setPermisos(await permisoUsuario(pathname))
  }

  useEffect(() => {
    definirPermisos().finally()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    obtenerProductosPeticion().finally(() => {})
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    pagina,
    limite,
    // eslint-disable-next-line react-hooks/exhaustive-deps
    JSON.stringify(ordenCriterios),
    filtroProducto,
  ])

  useEffect(() => {
    if (!mostrarFiltroProductos) {
      setFiltroProducto('')
    }
  }, [mostrarFiltroProductos])

  const paginacion = (
    <Paginacion
      pagina={pagina}
      limite={limite}
      total={total}
      cambioPagina={setPagina}
      cambioLimite={setLimite}
    />
  )

  return (
    <>
      <title>{`Productos - ${siteName()}`}</title>
      {/* <AlertDialog
        isOpen={mostrarAlertaEstadoProducto}
        titulo={'Alerta'}
        texto={`¿Está seguro de ${
          productoEdicion?.estado == 'ACTIVO' ? 'inactivar' : 'activar'
        } el producto: ${titleCase(productoEdicion?.nombre ?? '')} ?`}
      >
        <Button onClick={cancelarAlertaEstadoProducto}>Cancelar</Button>
        <Button onClick={aceptarAlertaEstadoProducto}>Aceptar</Button>
      </AlertDialog> */}
      <CustomDialog
        isOpen={modalDetallePedido}
        handleClose={cerrarModalDetallePedido}
        title={'Crear Pedido'}
      >
        <VistaModalDetallePedido
          idUsuario={`${usuario?.id}`}
          producto={productoEdicion}
          accionCorrecta={() => {
            cerrarModalDetallePedido().finally()
            obtenerProductosPeticion().finally()
          }}
          accionCancelar={cerrarModalDetallePedido}
        />
      </CustomDialog>
      <CustomDataTable
        titulo={'Productos'}
        error={!!errorProductosData}
        cargando={loading}
        acciones={acciones}
        columnas={ordenCriterios}
        cambioOrdenCriterios={setOrdenCriterios}
        paginacion={paginacion}
        contenidoTabla={contenidoTabla}
        filtros={
          mostrarFiltroProductos && (
            <FiltroProductos
              filtroProducto={filtroProducto}
              accionCorrecta={(filtros) => {
                setPagina(1)
                setLimite(10)
                setFiltroProducto(filtros.producto)
              }}
              accionCerrar={() => {
                imprimir(`👀 cerrar`)
              }}
            />
          )
        }
      />
    </>
  )
}
