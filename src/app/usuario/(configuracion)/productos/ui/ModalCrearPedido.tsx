import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { Button, DialogActions, DialogContent, Grid, Typography } from '@mui/material'
import { useAlerts, useSession } from '@/hooks'
import { delay, InterpreteMensajes } from '@/utils'
import { Constantes } from '@/config/Constantes'
import { imprimir } from '@/utils/imprimir'
import { CrearPedido } from '../../pedidos/types/pedidosCRUDTypes'
import { useAuth } from '@/context/AuthProvider'

export interface ModalPedidoType {
  pedido?: CrearPedido | null
  accionCorrecta: () => void
  accionCancelar: () => void
}

export const VistaModalPedido = ({
  accionCorrecta,
  accionCancelar,
}: ModalPedidoType) => {
  const [loadingModal, setLoadingModal] = useState<boolean>(false)

  // Hook para mostrar alertas
  const { Alerta } = useAlerts()

  // Obtener el usuario
  const {usuario}  = useAuth()

  // Proveedor de la sesión
  const { sesionPeticion } = useSession()

  const { handleSubmit } = useForm<CrearPedido>({
    defaultValues: {
        idUsuario: usuario?.id,
        costo: 0  
    },
  })

  const guardarPedido = async (
    data: CrearPedido
  ) => {
    await guardarPedidoPeticion(data)
  }

  const guardarPedidoPeticion = async (
    pedido: CrearPedido
  ) => {
    try {
      setLoadingModal(true)
      await delay(1000)
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/pedidos`,
        tipo: 'post',
        body: pedido,
      })
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: 'success',
      })
      accionCorrecta()
    } catch (e) {
      imprimir(`Error al crear o actualizar parámetro`, e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    } finally {
      setLoadingModal(false)
    }
  }

  return (
    <form onSubmit={handleSubmit(guardarPedido)}>
      <DialogContent dividers>
        <Grid container direction={'column'} justifyContent="space-evenly">
        <Grid item xs={12} sm={12} md={6}>
              <Typography variant="h6">
                Desea crear un nuevo carrito de compras?
              </Typography>
            </Grid>
        </Grid>
      </DialogContent>
      <DialogActions
        sx={{
          my: 1,
          mx: 2,
          justifyContent: {
            lg: 'flex-end',
            md: 'flex-end',
            xs: 'center',
            sm: 'center',
          },
        }}
      >
        <Button
          variant={'outlined'}
          disabled={loadingModal}
          onClick={accionCancelar}
        >
          Cancelar
        </Button>
        <Button variant={'contained'} disabled={loadingModal} type={'submit'}>
          Aceptar
        </Button>
      </DialogActions>
    </form>
  )
}
