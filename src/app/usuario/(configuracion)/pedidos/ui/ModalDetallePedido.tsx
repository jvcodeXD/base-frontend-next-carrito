import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { Box, Button, DialogActions, DialogContent, Grid, Typography } from '@mui/material'
import {
    CrearEditarDetallePedidoCRUDType,
  DetallePedidoCRUDType,
} from '@/app/usuario/(configuracion)/pedidos/types/pedidosCRUDTypes'
import { useAlerts, useSession } from '@/hooks'
import { delay, InterpreteMensajes } from '@/utils'
import { Constantes } from '@/config/Constantes'
import { imprimir } from '@/utils/imprimir'
import { FormInputText } from 'src/components/form'
import ProgresoLineal from '@/components/progreso/ProgresoLineal'
import { ProductoCRUDType } from '@/app/admin/(configuracion)/productos/types/productosCRUDTypes'

export interface ModalDetallePedidoType {
  idUsuario: string
  producto: ProductoCRUDType | null | undefined
  detallePedido?: DetallePedidoCRUDType | null
  accionCorrecta: () => void
  accionCancelar: () => void
}

export const VistaModalDetallePedido = ({
  idUsuario,
  producto,
  detallePedido,
  accionCorrecta,
  accionCancelar,
}: ModalDetallePedidoType) => {
  const [loadingModal, setLoadingModal] = useState<boolean>(false)

  // Hook para mostrar alertas
  const { Alerta } = useAlerts()

  // Proveedor de la sesión
  const { sesionPeticion } = useSession()

  const { handleSubmit, control } = useForm<CrearEditarDetallePedidoCRUDType>({
    defaultValues: {
      id: detallePedido?.id,
      idProducto: detallePedido?.idProducto || producto?.id,
      idPedido: detallePedido?.idPedido,
      idUsuario: detallePedido?.idUsuario || idUsuario
    },
  })

  const guardarActualizarDetallePedido = async (
    data: CrearEditarDetallePedidoCRUDType
  ) => {
    await guardarActualizarDetallePedidoPeticion(data)
  }

  const guardarActualizarDetallePedidoPeticion = async (
    detallePedido: CrearEditarDetallePedidoCRUDType
  ) => {
    try {
      setLoadingModal(true)
      await delay(1000)
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/pedidos/detalle${
          detallePedido.id ? `/${detallePedido.id}` : ''
        }`,
        tipo: !!detallePedido.id ? 'patch' : 'post',
        body: detallePedido,
      })
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: 'success',
      })
      accionCorrecta()
    } catch (e) {
      imprimir(`Error al crear o actualizar parámetro`, e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    } finally {
      setLoadingModal(false)
    }
  }

  return (
    <form onSubmit={handleSubmit(guardarActualizarDetallePedido)}>
      <DialogContent dividers>
        <Grid container direction={'column'} justifyContent="space-evenly">
          <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
            <Grid item xs={12} sm={12} md={6}>
              <Typography
                variant='body1'
              >
                Desea agregar al carrito el producto P?
              </Typography>
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
              <FormInputText
                id={'cantidad'}
                control={control}
                name="cantidad"
                label="Cantidad"
                disabled={loadingModal}
                rules={{ required: 'Este campo es requerido' }}
              />
            </Grid>
          </Grid>
          <Box height={'10px'} />
          <ProgresoLineal mostrar={loadingModal} />
          <Box height={'5px'} />
        </Grid>
      </DialogContent>
      <DialogActions
        sx={{
          my: 1,
          mx: 2,
          justifyContent: {
            lg: 'flex-end',
            md: 'flex-end',
            xs: 'center',
            sm: 'center',
          },
        }}
      >
        <Button
          variant={'outlined'}
          disabled={loadingModal}
          onClick={accionCancelar}
        >
          Cancelar
        </Button>
        <Button variant={'contained'} disabled={loadingModal} type={'submit'}>
          Guardar
        </Button>
      </DialogActions>
    </form>
  )
}
