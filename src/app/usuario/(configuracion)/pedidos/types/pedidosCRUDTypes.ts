/// CRUD de detalle de pedidos

export interface DetallePedidoCRUDType {
    id: string 
    idProducto: string 
    idPedido: string
    idUsuario: string 
    cantidad: number 
}

export interface CrearEditarDetallePedidoCRUDType {
    id?: string 
    idProducto: string 
    idPedido: string
    idUsuario: string 
    cantidad: number 
}

export interface CrearPedido {
    idUsuario: string
    costo: number
}

