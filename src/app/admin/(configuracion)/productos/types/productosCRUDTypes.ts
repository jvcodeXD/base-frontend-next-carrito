/// CRUD de productos

export interface ProductoCRUDType {
    id: string
    codigo: string
    nombre: string
    precio: number
    cantidad: number
    descripcion: string
    estado: string
  }
  
  export interface CrearEditarProductoCRUDType {
    id?: string
    codigo: string
    nombre: string
    precio: number 
    cantidad: number
    descripcion: string
  }
  